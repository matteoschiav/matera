
function [] = MCSgotoZero(handle,channel,hold)
    channel = channel-1;
    out = calllib('MCSControl', 'SA_FindReferenceMark_S',uint32(handle),uint32(channel),uint32(2),uint32(hold),uint32(1));
    
    if out ~= 0
        disp('MCSControl error');
    end