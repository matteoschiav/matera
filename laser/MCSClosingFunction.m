%%%%%%%%%%%%%%%%%%%%%% MCS CLOSING FUNCTION %%%%%%%%%%%%%%%%%%%%%%

% This function closes disconnects quTau

function []=MCSClosingFunction()

out = calllib('MCSControl','SA_ReleaseSystems');
pause(2);
if out == 0
    disp('MCSControl session ended');
else
    errordlg('MCSControl closing error');
end

unloadlibrary('MCSControl');

%close all;
