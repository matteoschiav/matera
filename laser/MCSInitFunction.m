%%%%%%%%%%%%%%%%%%%%%% MCS INIT FUNCTION %%%%%%%%%%%%%%%%%%%%%%
% This function does not require input nor produces output. It simply
% connects your PC to MCS device. It has to be launched once at the
% moment of starting using MCS.

function []=MCSInitFunction()


[notfound1,warnings1]=loadlibrary('C:\SmarAct\MCS\SDK\lib\MCSControl.dll','C:\SmarAct\MCS\SDK\include\MCSControl.h','alias','MCSControl');


out = calllib('MCSControl','SA_InitSystems', uint32(0));

if out==0
    disp('MCSControl session started');
else
    errordlg('MCSControl in not connected');
    unloadlibrary('MCSControl');
end

pause(2);