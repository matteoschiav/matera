function [position]=MCSGetPosition(handle,channel)

position=0;
positionpoint = libpointer('int32Ptr', position);
out = calllib('MCSControl', 'SA_GetPosition_S',uint32(handle),uint32(channel),positionpoint);

if out ~= 0
    disp('MCSControl error');
end


position=positionpoint.Value;