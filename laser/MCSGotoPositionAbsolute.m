

function []=MCSGotoPositionAbsolute(handle,channel,position,hold)

channel = channel-1;

out = calllib('MCSControl', 'SA_GotoPositionAbsolute_S',uint32(handle),uint32(channel),int32(position),uint32(hold));

if out ~= 0
    disp('MCSControl error');
end