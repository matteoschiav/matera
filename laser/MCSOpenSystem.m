function systemHandle = MCSOpenSystem(systemID)
% 
try
    MCSCloseSystem(32)
catch
    
end



[notfound1,warnings1]=loadlibrary('C:\SmarAct\MCS\SDK\lib\MCSControl.dll','C:\SmarAct\MCS\SDK\include\MCSControl.h','alias','MCSControl');

ID = char(['usb:id:' num2str(systemID)]);
shandle = 0;
systemHandle = libpointer('uint32Ptr', shandle);
opt = 'sync';
out = calllib('MCSControl', 'SA_OpenSystem',systemHandle,ID,opt);

if out ~= 0
    disp('MCSControl error');
end
systemHandle = systemHandle.Value;

end