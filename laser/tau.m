function varargout = tau(varargin)
% TAU MATLAB code for tau.fig
%      TAU, by itself, creates a new TAU or raises the existing
%      singleton*.
%
%      H = TAU returns the handle to a new TAU or the handle to
%      the existing singleton*.
%
%      TAU('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in TAU.M with the given input arguments.
%
%      TAU('Property','Value',...) creates a new TAU or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before tau_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to tau_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help tau

% Last Modified by GUIDE v2.5 28-Apr-2015 16:49:38

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @tau_OpeningFcn, ...
                   'gui_OutputFcn',  @tau_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% global variables: x and count
x = [];
count = [];


% --- Executes just before tau is made visible.
function tau_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to tau (see VARARGIN)

% Choose default command line output for tau
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes tau wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = tau_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function a1_Callback(hObject, eventdata, handles)
% hObject    handle to a1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of a1 as text
%        str2double(get(hObject,'String')) returns contents of a1 as a double


% --- Executes during object creation, after setting all properties.
function a1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to a1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function a2_Callback(hObject, eventdata, handles)
% hObject    handle to a2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of a2 as text
%        str2double(get(hObject,'String')) returns contents of a2 as a double


% --- Executes during object creation, after setting all properties.
function a2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to a2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function a3_Callback(hObject, eventdata, handles)
% hObject    handle to a3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of a3 as text
%        str2double(get(hObject,'String')) returns contents of a3 as a double


% --- Executes during object creation, after setting all properties.
function a3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to a3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function a4_Callback(hObject, eventdata, handles)
% hObject    handle to a4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of a4 as text
%        str2double(get(hObject,'String')) returns contents of a4 as a double


% --- Executes during object creation, after setting all properties.
function a4_CreateFcn(hObject, eventdata, handles)
% hObject    handle to a4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function waist_Callback(hObject, eventdata, handles)
% hObject    handle to waist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of waist as text
%        str2double(get(hObject,'String')) returns contents of waist as a double


% --- Executes during object creation, after setting all properties.
function waist_CreateFcn(hObject, eventdata, handles)
% hObject    handle to waist (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in acqButton.
function acqButton_Callback(hObject, eventdata, handles)
% hObject    handle to acqButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    % Create visa object for the Thorlabs PM100D
    v = visa('tek','USB0::0x1313::0x8078::P0003682::INSTR');

    % Hint: get(hObject,'Value') returns toggle state of acqButton
    but = get(hObject,'Value');
    if but == get(hObject,'Max')
        % green button, signaling acquisition
        set(hObject,'BackgroundColor','green');
        
        % pause between successive readouts of the power meter
        acqPause = str2double(get(handles.pauseAcq,'String'));
        
        % sample counter
        sampleIndex = 1;
        sampleTot = 1000;
        % sample array
        sample = (1:sampleTot) .* (acqPause/1000);
        % power array
        power = zeros(sampleTot,1);
        
        % open the Thorlabs PM100D
        fopen(v);

        while but == get(hObject,'Max')
            
            % read last power value (in mW)
            fprintf(v,'READ?');
            p = str2double(fscanf(v))*1000;
            
            power(sampleIndex) = p*1000;
            sampleIndex = mod(sampleIndex,sampleTot) + 1;
            
            set(handles.lastPower,'String',num2str(p));
            
            plot(handles.ax_oscillo,sample,power,'.');
            xlabel('Time [s]');
            ylabel('Power [mW]');
            pause(acqPause/1000);
            
            but = get(hObject,'Value');
        end
        
        fclose(v);
        
        % give acqButton its default color
        set(hObject,'BackgroundColor',[0.941 0.941 0.941]);
        
    end


function min_Callback(hObject, eventdata, handles)
% hObject    handle to min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of min as text
%        str2double(get(hObject,'String')) returns contents of min as a double


% --- Executes during object creation, after setting all properties.
function min_CreateFcn(hObject, eventdata, handles)
% hObject    handle to min (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function max_Callback(hObject, eventdata, handles)
% hObject    handle to max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of max as text
%        str2double(get(hObject,'String')) returns contents of max as a double


% --- Executes during object creation, after setting all properties.
function max_CreateFcn(hObject, eventdata, handles)
% hObject    handle to max (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function step_Callback(hObject, eventdata, handles)
% hObject    handle to step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of step as text
%        str2double(get(hObject,'String')) returns contents of step as a double


% --- Executes during object creation, after setting all properties.
function step_CreateFcn(hObject, eventdata, handles)
% hObject    handle to step (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in startButton.
function startButton_Callback(hObject, eventdata, handles)
% hObject    handle to startButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global count
    global x

    % Create visa object for the Thorlabs PM100D
    v = visa('ni','USB0::0x1313::0x8078::P0003682::INSTR');

    % Hint: get(hObject,'Value') returns toggle state of startButton

    but = get(hObject,'Value');
    if but == get(hObject,'Max')
        
        % disable save button
        set(handles.savebutton,'Enable','off');
        
        average = str2double(get(handles.average,'String'));
        minStage = str2double(get(handles.min,'String'));
        maxStage = str2double(get(handles.max,'String'));
        step = str2double(get(handles.step,'String'));
        
        x = minStage:step:maxStage;
        count = zeros(1,length(x));
        
        fopen(v);
        
        h_Motor = MCSOpenSystem(2170511353);
        MCSGotoPositionAbsolute(h_Motor,1,minStage*1e6,60000);
        pause(1);
        
        for i = 1:length(x)
            MCSGotoPositionAbsolute(h_Motor,1,x(i)*1e6,60000);
            pause(0.1); % wait 100 ms before acquiring data
            singleMeasure = zeros(average,1);
            for j=1:average
                fprintf(v,'READ?');
                % power in mW
                p = str2double(fscanf(v))*1000;
                singleMeasure(j) = p;
            end
            count(i) = mean(singleMeasure);
            plot(handles.ax_vis,x,count,'.');
        end
        
        fclose(v);
                
        hold on
        plot(handles.ax_vis,x,f(par),'r');
        hold off
        
        set(handles.savebutton,'Enable','on');
                
        set(hObject,'BackgroundColor',[0.941 0.941 0.941]);
        
    end

function pauseAcq_Callback(hObject, eventdata, handles)
% hObject    handle to pauseAcq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of pauseAcq as text
%        str2double(get(hObject,'String')) returns contents of pauseAcq as a double


% --- Executes during object creation, after setting all properties.
function pauseAcq_CreateFcn(hObject, eventdata, handles)
% hObject    handle to pauseAcq (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function lastPower_Callback(hObject, eventdata, handles)
% hObject    handle to lastPower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of lastPower as text
%        str2double(get(hObject,'String')) returns contents of lastPower as a double


% --- Executes during object creation, after setting all properties.
function lastPower_CreateFcn(hObject, eventdata, handles)
% hObject    handle to lastPower (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function average_Callback(hObject, eventdata, handles)
% hObject    handle to average (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of average as text
%        str2double(get(hObject,'String')) returns contents of average as a double


% --- Executes during object creation, after setting all properties.
function average_CreateFcn(hObject, eventdata, handles)
% hObject    handle to average (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in savebutton.
function savebutton_Callback(hObject, eventdata, handles)
% hObject    handle to savebutton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

    global x count
    if (s)
        [Filename,Pathname] = uiputfile('*.mat','Save measurements as');
        if Filename
            save([Pathname Filename],'x','count');
        end
    end


% --- Executes during object deletion, before destroying properties.
function figure1_DeleteFcn(hObject, eventdata, handles)
% hObject    handle to figure1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
