%%%%% GET THE STATUS OF THE TRASLATOR %%%%%
% PARAMETERS: (systemHandle, channel)
% STATUS:
% 0 = STOP
% 1 = STEPPING
% 2 = SCANNING
% 3 = HOLDING
% 4 = The positioner or end effector is currently performing a closed-loop movement
% 5 = The positioner is currently waiting for the sensors to power-up before executing the movement command.
% 6 = CALIBRATING
% 7 = FINDING_REF
% 8 = The end effector (gripper) is closing or opening its jaws

function status1 = MCSGetStatus(handle,channel)
    status = 0;
    channel = channel-1;
    statuspointer = libpointer('uint32Ptr', status);
    out = calllib('MCSControl', 'SA_GetStatus_S',uint32(handle),uint32(channel),statuspointer);

    if out ~= 0
        disp('MCSControl error');
    end
    status = statuspointer.Value;
    if status==0 || status ==3
        status1 = 0;
    else
        status1 = 1;
    end