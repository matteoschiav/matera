
function []=MCSSetPosition(handle,channel,position)

channel = channel-1;
out = calllib('MCSControl', 'SA_SetPosition_S',uint32(handle),uint32(channel),int32(-position));

if out ~= 0
    disp('MCSControl error');
end