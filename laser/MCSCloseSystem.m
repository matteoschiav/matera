function [] = MCSCloseSystem(systemHandle)
    systemHandle = uint32(systemHandle);
    pause(0.5);
    out = calllib('MCSControl', 'SA_CloseSystem',systemHandle);

    if out ~= 0
        disp('MCSControl error');
    end

    unloadlibrary('MCSControl');
